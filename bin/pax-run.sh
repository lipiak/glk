SCRIPTS=`readlink $0`
if [ "${SCRIPTS}" != "" ]
then
  SCRIPTS=`dirname $SCRIPTS`
else
  SCRIPTS=`dirname $0`
fi

java $JAVA_OPTS -cp .:$SCRIPTS:$SCRIPTS/pax-runner-1.8.5.jar org.ops4j.pax.runner.Run "$@"