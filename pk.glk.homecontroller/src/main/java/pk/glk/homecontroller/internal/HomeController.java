package pk.glk.homecontroller.internal;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.osgi.framework.BundleContext;

import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.felix.scr.annotations.*;

//REQUIRED INTERFACES
//import pk.glk.contracts.ISendMail;
import pk.glk.contracts.IContactsController;
import pk.glk.contracts.IGLKProvider;
import pk.glk.contracts.IConfig;
import pk.glk.contracts.IConfigInfo;
import pk.glk.contracts.IConversationManager;
import pk.glk.contracts.IConversationProvider;
import pk.glk.contracts.IGUIModel;
import pk.glk.soundplayer.ISound;
import pk.glk.contracts.IHomeController;
import pk.glk.contracts.IMessage;
import pk.glk.contracts.IUser;

@Service
@Component(immediate=true)
public final class HomeController implements IHomeController
{
    private BundleContext context;
    private LoginWindow loginWindow;
    private ConversationWindow conversationWindow;
    private Thread sessionExtendThread;
    //private MainWindow mainWindow;

    //referencje opisane w pliku pom.xml w głównym katalogu HomeController
    //najlepiej najpierw wygenerować te wpisy referencjami @Component, @Service, @Reference
    //a potem pozmieniać w pom.xml odpowiednie pola
    //@Reference
    //private IConversationManager ConvMgr;		

    @Reference
    private IConfig Config;		

    @Reference
    private IGLKProvider GLK;		

    @Reference
    private IContactsController Contacts;

    @Reference
    private ISound Sound;
    
    @Reference 
    private IConversationManager ConvMgr;

    @Reference
    private IGUIModel GUIModel;
    
    @Reference
    private IConversationProvider CP;


    //connecting conversation manager component
    //protected void bindConvMgr(IConversationManager cm)
    //{
            //this.ConvMgr = cm;
    //}

    //connecting config component
    protected void bindConfig(IConfig config)
    {
            this.Config = config;
    }

    //connecting glkprovider component
    protected void bindGLK(IGLKProvider glk)
    {
            this.GLK = glk;
    }

    //connecting contacts component
    protected void bindContacts(IContactsController contacts)
    {
            this.Contacts = contacts;
    }

    //connect sound component
    protected void bindSound(ISound sound)
    {
            this.Sound = sound;
    }
    
    protected void bindConvMgr(IConversationManager cf)
    {

        this.ConvMgr = cf;

    }
    
    protected void bindGUIModel(IGUIModel model)
    {
        
        this.GUIModel = model;
    }
    
        
    protected void bindCP(IConversationProvider model)
    {
        
        this.CP = model;
    }

    @Activate
    public void activate(BundleContext bc)
    {
            this.context = bc;
         
            System.out.println("HomeController bundle activated");
    }


    @Override
    public int RegisterUser(IUser user) {
        return GLK.RegisterNewUser(user);
    }

    @Override
    public void LogIn(String number, String password) {
        String sessionHash = GLK.LogIn(number, password);
        if(sessionHash != null)
        {
            GUIModel.SetLoggedIn(true);
            Sound.PlaySound("Sounds/login.wav", true);
            Config.SetConfig(Integer.parseInt(number), password, true);
            ConvMgr.GetAllMessages();
            
            //start thread to keep connection alive
            new Thread(){public void run()
            {
                //żeby można było później zatrzymać wątek np przy wylogowaniu
                sessionExtendThread = this;
                while(true)
                {
                        try {
                            sleep(10000);
                        } catch (InterruptedException ex) {
                            System.out.println(ex.getMessage());
                            return;                      
                        }

                        //jeśli nei udało się przedłużyć sesji, zatrzymaj sprawdzanie
                        if(ExtendSession() == false)
                        {
                            //ConvMgr.StopReceiving();
                            this.interrupt();                          
                        }
                        ConvMgr.GetAllMessages();
                }
             }}.start();
            
        }
        else GUIModel.SetLoggedIn(false);
    }
    
    private boolean ExtendSession()
    {
        System.out.println("Extending session");
        if(!GLK.CheckAuthorization())
        {
            GUIModel.SetLoggedIn(false);
            System.out.println("Failed to extend");
            return false;
        }
        
        else
        { 
            System.out.println("Session extended");
            return true;
        }
    }

    @Override
    public void LogOut() {
        
        IConfigInfo cfg = Config.GetConfig();
        if(GLK.LogOut(String.valueOf(cfg.GetNumber()), cfg.GetPassword()))
        {
            sessionExtendThread.interrupt();
            System.out.println("Thread should be interrupted now");
            GUIModel.SetLoggedIn(false);   
        }
               
    }

    @Override
    public void StartNewConversation(int number) {
        GUIModel.NewConversation(number);
    }

    @Override
    public Boolean ChangeStatus(Boolean IsOnline) {
        return GLK.ChangeStatus(IsOnline);
    }

    @Override
    public void OpenFriendArchive(int FriendGLKNumber) {
        //System.out.println("----- Archive HC with: " + FriendGLKNumber);
        List<IMessage> l = CP.RecieveFriendRegistry(FriendGLKNumber);
        
        GUIModel.FriendArchive(l);
    }
}

