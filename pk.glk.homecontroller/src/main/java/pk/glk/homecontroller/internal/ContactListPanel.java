/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.glk.homecontroller.internal;

import java.awt.Color;
import java.awt.Dimension;
import org.osgi.framework.BundleContext;
import org.apache.felix.scr.annotations.*;
import pk.glk.contracts.IContactsController;
import pk.glk.contracts.IGUIModel;
import pk.glk.contracts.IHomeController;



/**
 *
 * @author Piotrek
 */
//@Component
public class ContactListPanel extends javax.swing.JPanel {

    private int number;
    private String showedName;
    private int status; //
    /**
     * Creates new form ContactListPanel
     */
    
    //@Reference
    private IContactsController CC;
    private IHomeController HC;
    
    
    private javax.swing.JPanel pnlContactsOnList;
        

    protected void bindCC(IContactsController cc) {
        this.CC = cc;
        System.out.println(CC);
        //no i jest ok
    }
    
    //@Activate
    public void activate(BundleContext bc)
    {
        
    }
    
    
    public ContactListPanel(){
        initComponents();
    }
    
    public ContactListPanel(int number, String nameOnList, int status, IContactsController c, javax.swing.JPanel jp, IHomeController HC) {
        initComponents();
        this.number = number;
        this.showedName = nameOnList;
        this.status = status;
        this.lblContactName.setText(nameOnList);
        this.setContactStatus(status);
        this.CC = c;
        this.pnlContactsOnList = jp;
        this.HC = HC;
        this.setMaximumSize(new Dimension(292,64));
    }
    
    public void setNumber(int Number)
    {
        this.number = Number;
    }
    
    public int getNumber()
    {
        return this.number;
    }
    
    public void setContactStatus(int status)
    {
        if(status == 0)//chwilowo tak oznaczam offline
        {
            this.lblSendFile.setVisible(false);
            this.lblSeparator2.setVisible(false);
            this.pnlStatus.setBackground(Color.red);
        }
        
        if(status == 1)//dostepny
        {
            this.lblSendFile.setVisible(true);
            this.lblSeparator2.setVisible(true);
            this.pnlStatus.setBackground(Color.green);
        }
        
        if(status == 2)//AFK
        {
            this.lblSendFile.setVisible(true);
            this.lblSeparator2.setVisible(true);
            this.pnlStatus.setBackground(Color.yellow);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        pnlStatus = new javax.swing.JPanel();
        lblContactName = new javax.swing.JLabel();
        pnlContactActions = new javax.swing.JPanel();
        lblWrite = new javax.swing.JLabel();
        lblSeparator = new javax.swing.JLabel();
        lblArchive = new javax.swing.JLabel();
        lblSeparator1 = new javax.swing.JLabel();
        lblDelete = new javax.swing.JLabel();
        lblSeparator2 = new javax.swing.JLabel();
        lblSendFile = new javax.swing.JLabel();

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        pnlStatus.setBackground(new java.awt.Color(204, 204, 255));

        javax.swing.GroupLayout pnlStatusLayout = new javax.swing.GroupLayout(pnlStatus);
        pnlStatus.setLayout(pnlStatusLayout);
        pnlStatusLayout.setHorizontalGroup(
            pnlStatusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 44, Short.MAX_VALUE)
        );
        pnlStatusLayout.setVerticalGroup(
            pnlStatusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        lblContactName.setFont(new java.awt.Font("Comic Sans MS", 0, 18)); // NOI18N
        lblContactName.setText("Contact name");

        lblWrite.setFont(new java.awt.Font("Comic Sans MS", 0, 11)); // NOI18N
        lblWrite.setForeground(new java.awt.Color(51, 153, 255));
        lblWrite.setText("<HTML><U>Write</U></HTML>");
        lblWrite.setToolTipText("");
        lblWrite.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblWrite.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                onWrite(evt);
            }
        });

        lblSeparator.setFont(new java.awt.Font("Comic Sans MS", 0, 11)); // NOI18N
        lblSeparator.setText("|");
        lblSeparator.setToolTipText("");
        lblSeparator.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        lblArchive.setFont(new java.awt.Font("Comic Sans MS", 0, 11)); // NOI18N
        lblArchive.setForeground(new java.awt.Color(51, 153, 255));
        lblArchive.setText("<HTML><U>Archive</U></HTML>");
        lblArchive.setToolTipText("");
        lblArchive.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblArchive.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                onArchive(evt);
            }
        });

        lblSeparator1.setFont(new java.awt.Font("Comic Sans MS", 0, 11)); // NOI18N
        lblSeparator1.setText("|");
        lblSeparator1.setToolTipText("");
        lblSeparator1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        lblDelete.setFont(new java.awt.Font("Comic Sans MS", 0, 11)); // NOI18N
        lblDelete.setForeground(new java.awt.Color(51, 153, 255));
        lblDelete.setText("<HTML><U>Delete</U></HTML>");
        lblDelete.setToolTipText("");
        lblDelete.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblDelete.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                onDelete(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                onDeleteMousePressed(evt);
            }
        });

        lblSeparator2.setFont(new java.awt.Font("Comic Sans MS", 0, 11)); // NOI18N
        lblSeparator2.setText("|");
        lblSeparator2.setToolTipText("");
        lblSeparator2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        lblSendFile.setFont(new java.awt.Font("Comic Sans MS", 0, 11)); // NOI18N
        lblSendFile.setForeground(new java.awt.Color(51, 153, 255));
        lblSendFile.setText("<HTML><U>Send file</U></HTML>");
        lblSendFile.setToolTipText("");
        lblSendFile.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblSendFile.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                onSendFile(evt);
            }
        });

        javax.swing.GroupLayout pnlContactActionsLayout = new javax.swing.GroupLayout(pnlContactActions);
        pnlContactActions.setLayout(pnlContactActionsLayout);
        pnlContactActionsLayout.setHorizontalGroup(
            pnlContactActionsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlContactActionsLayout.createSequentialGroup()
                .addComponent(lblWrite, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblSeparator)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblArchive, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblSeparator1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblDelete, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblSeparator2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblSendFile, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        pnlContactActionsLayout.setVerticalGroup(
            pnlContactActionsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlContactActionsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lblWrite, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblSeparator)
                .addComponent(lblArchive, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblSeparator1)
                .addComponent(lblDelete, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblSeparator2)
                .addComponent(lblSendFile, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnlStatus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(27, 27, 27)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblContactName)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(pnlContactActions, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblContactName, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(4, 4, 4)
                        .addComponent(pnlContactActions, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(pnlStatus, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void onWrite(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_onWrite
        //System.out.println("----- conversation started with: " + number);
        this.HC.StartNewConversation(this.number);
    }//GEN-LAST:event_onWrite

    private void onArchive(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_onArchive
        System.out.println("----- Archive started with: " + number);
        this.HC.OpenFriendArchive(this.number);
    }//GEN-LAST:event_onArchive

    private void onDelete(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_onDelete
        // TODO add your handling code here:
    }//GEN-LAST:event_onDelete

    private void onSendFile(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_onSendFile
        // TODO add your handling code here:
    }//GEN-LAST:event_onSendFile

    private void onDeleteMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_onDeleteMousePressed
       // System.out.print("-----" + CC + "-------"); 
        if(CC.DeleteContact(String.valueOf(this.number))) 
        {
            pnlContactsOnList.remove(this);
            this.remove(this); 
            pnlContactsOnList.revalidate();
            pnlContactsOnList.repaint();
        }

    }//GEN-LAST:event_onDeleteMousePressed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel lblArchive;
    private javax.swing.JLabel lblContactName;
    private javax.swing.JLabel lblDelete;
    private javax.swing.JLabel lblSendFile;
    private javax.swing.JLabel lblSeparator;
    private javax.swing.JLabel lblSeparator1;
    private javax.swing.JLabel lblSeparator2;
    private javax.swing.JLabel lblWrite;
    private javax.swing.JPanel pnlContactActions;
    private javax.swing.JPanel pnlStatus;
    // End of variables declaration//GEN-END:variables
}
