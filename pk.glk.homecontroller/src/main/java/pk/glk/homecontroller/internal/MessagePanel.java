/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.glk.homecontroller.internal;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author Patryk
 */
class MessagePanel extends JPanel {
    private JLabel lblAuthor;
    private JLabel lblMessage;
    
    public MessagePanel(String author, String message)
    {
        this.setMinimumSize(new Dimension(50, 25));
        this.setPreferredSize(new Dimension(100, 100));
        this.setMaximumSize(new Dimension(Short.MAX_VALUE,
                                  100));
        BorderLayout bl = new BorderLayout(20,0);
        this.setLayout(bl);
        lblAuthor = new JLabel();
        lblAuthor.setText(author);
        lblMessage = new JLabel();
        lblMessage.setText(message);
        this.add(lblAuthor, BorderLayout.LINE_START);
        this.add(lblMessage, BorderLayout.CENTER);
        
        if(author != "Me:")
            this.setBackground(new Color(245,245,245));
        
        this.setVisible(true);
    }

}
