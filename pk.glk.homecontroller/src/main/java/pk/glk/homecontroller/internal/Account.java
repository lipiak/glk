/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.glk.homecontroller.internal;

import pk.glk.contracts.IAccount;
/**
 *
 * @author Piotrek
 */
public class Account implements IAccount {
    
        private final int GLKNumber;
        private final String Firstname;
        private final String Surname;
        private final String Email;
        private final String BirthDay;
        private final String UserIP;
        private final Boolean isOnline;
        
        public Account(int GLKNumber, String Firstname, String Surname, String Email, String Birthday, String UserIP, Boolean isOnline)
        {
            this.GLKNumber = GLKNumber;
            this.Email = Email;
            this.Firstname = Firstname;
            this.Surname = Surname;
            this.BirthDay = Birthday;
            this.isOnline = isOnline;
            this.UserIP = UserIP;
        }
        
        @Override
        public int getGLKNumber() {
            return GLKNumber;
        }
        
        @Override
        public String getFirstname() {
            return Firstname;
        }
        
        @Override
        public String getSurname() {
            return this.Surname;
        }
        
        @Override
        public String getEMail() {
            return this.Email;
        }
        
        @Override
        public String getBirthday() {
            return this.BirthDay;
        }
        
        @Override
        public String getUserIP() {
            return this.UserIP;
        }
        
        @Override
        public Boolean getStatus() {
            return this.isOnline;
        }
        
}
