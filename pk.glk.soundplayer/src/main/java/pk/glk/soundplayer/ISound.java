package pk.glk.soundplayer;

public interface ISound
{
    void PlaySound(String fileName, boolean multithreaded);
    void StopPlaying();
}

