package pk.glk.encryptor.internal;

import org.osgi.framework.BundleContext;
import org.apache.felix.scr.annotations.*;
import java.lang.String;

import pk.glk.contracts.IEncrypte;

@Component
@Service
public final class Encrypte
    implements IEncrypte
{
	private BundleContext context;
    
	@Activate
	public void activate(BundleContext bc)
	{
		this.context = bc;
		System.out.println("Encryptor bundle activated");
	}
	
	@Override
	public String Encryptor(String contentForDecode)
	{
		return "";
	}
	
	@Override
	public String Decryptor(String contentForEncode)
	{
		return "";
	}
    
}

