/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.glk.glkprovider.internal;

import pk.glk.contracts.IMessage;

/**
 *
 * @author Patryk
 */
public class Message implements IMessage {
    
    private final int senderNumber;
    private final String message;
    private final int receiverNumber;
    private final String sendDate;
    
    public Message(int sender, String message, int receiver, String date)
    {
        this.senderNumber = sender;
        this.message = message;
        this.receiverNumber = receiver;
        this.sendDate = date;
    }

    @Override
    public int GetSenderNumber() {
        return this.senderNumber;
    }

    @Override
    public String GetMessage() {
        return this.message;
    }

    @Override
    public int GetReceiverNumber() {
        return this.receiverNumber;
    }

    @Override
    public String GetSendDate() {
        return this.sendDate;
    }
    
    
}
