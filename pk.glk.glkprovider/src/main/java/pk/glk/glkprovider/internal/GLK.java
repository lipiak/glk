package pk.glk.glkprovider.internal;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import org.osgi.framework.BundleContext;
import org.apache.felix.scr.annotations.*;

import java.lang.String;
import pk.glk.contracts.IGLKProvider;
import pk.glk.contracts.IUser;
import pk.glk.contracts.IContactManager;
import java.util.List;
import pk.glk.contracts.IContactInfo;
import pk.glk.contracts.IMessage;
import pk.glk.contracts.IConversationProvider;
import pk.glk.contracts.IConfig;
import pk.glk.logger.ILogger;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import pk.glk.contracts.IConfigInfo;

@Component
@Service
public final class GLK
        implements IGLKProvider, IContactManager, IConversationProvider {

    private BundleContext context;

    @Reference
    private ILogger Logger;

    @Reference
    private IConfig Config;

    private HttpURLConnection connection;
    private String sessionToken;
    private String GLKNumber;
    private String password;

    @Activate
    public void activate(BundleContext bc) {
        this.context = bc;
        System.out.println("GLKProvider bundle activated");

    }

    @Override
    public int RegisterNewUser(IUser newUser) {
        //GENERATE XML MESSAGE
        String message
                = "<User><Email>" + newUser.getEMail() + "</Email><Firstname>" + newUser.getName()
                + "</Firstname><Password>" + newUser.getPassword() + "</Password><Surname>"
                + newUser.getSurname() + "</Surname></User>";

        //SEND AND RECEIVE MESSAGE ALSO IN XML FORMAT
        String result = SendPOSTData("http://glk.pl.hostingasp.pl/Services/Registration.svc/RegisterUser", message);
        //System.out.print(result);
        Document doc = this.ConvertStringToXML(result);
        String number = doc.getElementsByTagName("GLKNumber").item(0).getTextContent();
        return Integer.parseInt(number);
    }

    @Override
    public synchronized boolean CheckAuthorization() {
        try
        {
            String result = SendGETData("http://glk.pl.hostingasp.pl/Services/Login.svc/ExtendSession/", this.sessionToken);
            Document doc = ConvertStringToXML(result);
            String value = doc.getElementsByTagName("boolean").item(0).getTextContent();
            return Boolean.valueOf(value);
        }
        catch(Exception e)
        {
            return false;
        }
    }

    @Override
    public Boolean ChangeStatus(Boolean isOnline) {
        String message = this.sessionToken + "/" + String.valueOf(isOnline);
        String result = SendGETData("http://glk.pl.hostingasp.pl/Services/Login.svc/ExtendSession/", message );
        Document doc = ConvertStringToXML(result);
        String value = doc.getElementsByTagName("boolean").item(0).getTextContent();
        return Boolean.valueOf(value);
    }

    @Override
    public String LogIn(String number, String password) {
        Logger.WriteLine("LogIn()", "GLKProvider");
        try {
            String message
                    = "<User><GLKNumber>" + number + "</GLKNumber><Password>" + password + "</Password><UserIP>" + this.GetExternalIP() + "</UserIP></User>";
            String result = this.SendPOSTData("http://glk.pl.hostingasp.pl/Services/Login.svc/LoginUser", message);
            Document doc = ConvertStringToXML(result);
            String hash = doc.getElementsByTagName("string").item(0).getTextContent();
            System.out.println(hash);

            this.sessionToken = hash;
            this.password = password;
            this.GLKNumber = number;

            return hash;
        } catch (IOException ex) {
            System.out.print(ex.getMessage());
            return null;
        }
    }

    @Override
    public Boolean DeleteContact(String number) {
        
        String message = this.password + "/" + this.GLKNumber + "/" + number;
        String result = SendGETData("http://glk.pl.hostingasp.pl/Services/Friends.svc/DeleteFriend/", message);
        //System.out.print(result);
        Document doc = ConvertStringToXML(result);
        Boolean b = Boolean.parseBoolean(doc.getElementsByTagName("boolean").item(0).getTextContent());
        return b;
    }

    @Override
    public void AddContact(String number) {
        //IConfigInfo c = Config.GetConfig();
        String message = this.password + "/" + this.GLKNumber + "/" + number;
        String result = SendGETData("http://glk.pl.hostingasp.pl/Services/Friends.svc/AddFriend/", message);
        //System.out.print("----" + result + "----");
        //Document doc = ConvertStringToXML(result);
        //String email = doc.getElementsByTagName("Email").item(0).getTextContent();
        //String firstname = doc.getElementsByTagName("Firstname").item(0).getTextContent();
        //int glknumber = Integer.parseInt(doc.getElementsByTagName("GLKNumber").item(0).getTextContent());
        //Boolean online = Boolean.parseBoolean(doc.getElementsByTagName("Online").item(0).getTextContent());
        //String surname = doc.getElementsByTagName("Surname").item(0).getTextContent();
        //String userIP = doc.getElementsByTagName("UserIP").item(0).getTextContent();

        //IContactInfo contact = new Contact(glknumber, firstname, surname, email, null, null, online);
        //return contact;
    }

    @Override
    public List<IContactInfo> ReturnContactList() {
        //IConfigInfo c = Config.GetConfig();
        String message = this.password + "/" + this.GLKNumber;
        String result = SendGETData("http://glk.pl.hostingasp.pl/Services/Friends.svc/FriendsList/", message);
        
        System.out.println("Contact list refreshed");

        Document doc = ConvertStringToXML(result);

        NodeList list = doc.getElementsByTagName("Account");

        List<IContactInfo> contactList = new ArrayList<IContactInfo>();

		//System.out.println("Fręds : " + list.getLength());
        //int accountsCount = list.getLength();
        for (int i = 0; i < list.getLength(); i++) {
            //System.out.println(i);
            String email = doc.getElementsByTagName("Email").item(i).getTextContent();
            String firstname = doc.getElementsByTagName("Firstname").item(i).getTextContent();
            int glknumber = Integer.parseInt(doc.getElementsByTagName("GLKNumber").item(i).getTextContent());
            Boolean online = Boolean.parseBoolean(doc.getElementsByTagName("Online").item(i).getTextContent());
            String surname = doc.getElementsByTagName("Surname").item(i).getTextContent();
            String userIP = doc.getElementsByTagName("UserIP").item(i).getTextContent();

            IContactInfo contact = new Contact(glknumber, firstname, surname, email, null, userIP, online);
            contactList.add(contact);
        }

        return contactList;

    }

    @Override
    public String ReturnUserIP(IContactInfo contact) {
        return "";
    }

    @Override
    public List<IMessage> ReturnUserArchive(IContactInfo contact) {
        return null;
    }

    @Override
    public boolean SendMessage(IMessage message) {
        IConfigInfo c = Config.GetConfig();
                String query = 
                        "<Message>"
                        + "<MessageCnt>" + message.GetMessage() + "</MessageCnt>"
                        + "<ReceiverGLKNumber>" + String.valueOf(message.GetReceiverNumber())+ "</ReceiverGLKNumber>"
                        + "<SenderGLKNumber>" + String.valueOf(c.GetNumber()) + "</SenderGLKNumber></Message>";
                
                String result = SendPOSTData("http://glk.pl.hostingasp.pl/Services/Messenger.svc/SendMessage/" + this.sessionToken 
                        + "/" + c.GetPassword() + "/" + c.GetNumber(),query);

                Document doc = ConvertStringToXML(result);
                
                String returned_message = doc.getElementsByTagName("MessageCnt").item(0).getTextContent();
                
                if(returned_message != null && returned_message == message.GetMessage())
                    return true;
                else return false;
                
                //System.out.println("---------------- " + doc.getElementsByTagName("boolean").item(0).getTextContent());
                //Boolean b = Boolean.valueOf(doc.getElementsByTagName("boolean").item(0).getTextContent());
                
    }

    @Override
    public IMessage ReciveMessage(IContactInfo contact) {
        return null;
    }

    @Override
    public List<IMessage> ReciveAllMessages() {
        IConfigInfo c = Config.GetConfig();
		List<IMessage> messages = new ArrayList<>();
                
                String query = this.sessionToken + ""
                        + "/" + c.GetPassword() + "/" + String.valueOf(c.GetNumber());
                
                String result = SendGETData("http://glk.pl.hostingasp.pl/Services/Messenger.svc/GetMessages/",query);
                Document doc = ConvertStringToXML(result);
                
                //wszystkie wiadomosci
                if(doc == null)
                    return messages;
                
                NodeList received_messages = doc.getElementsByTagName("Message");
                
                //iterujemy po kazdej wiadomosci
                IMessage msg;
                int sender, receiver;
                String date, content;
                for (int i = 0; i < received_messages.getLength(); i++) {
                    
                    Element e = (Element)received_messages.item(i);
                    sender = Integer.parseInt(e.getElementsByTagName("SenderGLKNumber").item(0).getTextContent());
                    receiver = Integer.parseInt(e.getElementsByTagName("ReceiverGLKNumber").item(0).getTextContent());
                    date = e.getElementsByTagName("SendDate").item(0).getTextContent();
                    content = e.getElementsByTagName("MessageCnt").item(0).getTextContent();
                    msg = new Message(sender, content, receiver, date);
                    messages.add(msg);             
                }              
                return messages;
    }

    @Override
    public String GetSecretKey() {
        return "";
    }

    //connect Logger component
    public void bindLogger(ILogger logger) {
        this.Logger = logger;
    }

    //connect config component
    public void bindConfig(IConfig config) {
        this.Config = config;
    }

    private String SendPOSTData(String address, String data) {

        try {
            URL url = new URL(address);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type",
                    "application/xml; charset=utf-8");

            connection.setRequestProperty("Content-Length", ""
                    + Integer.toString(data.getBytes().length));
            connection.setRequestProperty("Content-Language", "en-US");

            connection.setUseCaches(false);
            connection.setDoInput(true);
            connection.setDoOutput(true);

            DataOutputStream wr = new DataOutputStream(
                    connection.getOutputStream());
            wr.writeBytes(data);
            wr.flush();
            wr.close();

            //Get Response	
            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            String line;
            StringBuffer response = new StringBuffer();
            while ((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }
            rd.close();
            return response.toString();
        } catch (Exception e) {
            return e.getMessage();
        }
    }

    private Document ConvertStringToXML(String result) {
            //PARSE MESSAGE, GET GENERATED NUMBER AND RETURN IT
        //Document doc = convertStringToDocument(result);
        //File fXmlFile = new File("/Users/mkyong/staff.xml"
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder;
        try {
            dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(new InputSource(new StringReader(result)));
            return doc;
                        //bierzemy GLKNUMBER

        } catch (ParserConfigurationException ex) {
            System.out.print(ex.getMessage());
            return null;
        } catch (SAXException ex) {
            System.out.print(ex.getMessage());
            return null;
        } catch (IOException ex) {
            System.out.print(ex.getMessage());
            return null;
        }
    }

    private String GetExternalIP() throws MalformedURLException, IOException {
        //<html><head><title>Current IP Check</title></head><body>Current IP Address: 77.255.60.108</body></html>
        URL whatismyip = new URL("http://checkip.dyndns.org");
        BufferedReader in = new BufferedReader(new InputStreamReader(
                whatismyip.openStream()));

        //podziel po spacjach, wez ostatni element
        String[] poSplicie = in.readLine().split(" ");
        String ostatni = poSplicie[poSplicie.length - 1];

            //77.255.60.108</body></html>
        //odciac koncowke
        return ostatni.substring(0, ostatni.length() - 14);
    }

    private synchronized String SendGETData(String address, String message) {
        try {
            URL url = new URL(address + message);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");

            //Get Response	
            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            String line;
            StringBuffer response = new StringBuffer();
            while ((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }
            rd.close();
            return response.toString();
        } catch (Exception e) {
            return e.getMessage();
        }
    }

    @Override
    public boolean LogOut(String number, String password) {

        try {
            String message = "<User><GLKNumber>" + number + "</GLKNumber><Password>"
                    + password + "</Password><UserIP>"
                    + this.GetExternalIP() + "</UserIP></User>";

            String result = SendPOSTData("http://glk.pl.hostingasp.pl/Services/Login.svc/UnlogUser/" + sessionToken, message);
            Document doc = this.ConvertStringToXML(result);
            String value = doc.getElementsByTagName("boolean").item(0).getTextContent();
            return Boolean.valueOf(value);

        } catch (IOException ex) {
            System.out.println(ex.getMessage());
            return false;
        }
    }

    @Override
    public IMessage CreateNewMessage(int sender, String message, int receiver, String date) {
        return new Message(sender,message,receiver,date);
    }

    @Override
    public List<IMessage> RecieveFriendRegistry(int FriendGLKNumber) {
        
                IConfigInfo c = Config.GetConfig();
		List<IMessage> messages = new ArrayList<>();
                
                String query = this.sessionToken + ""
                        + "/" + c.GetPassword() + "/" + String.valueOf(c.GetNumber()) + "/" + FriendGLKNumber;
                ///GetMessages/{SessionToken}/{OwnerPassword}/{OwnerGLKNumber}/{FriendGLKNumber}
                String result = SendGETData("http://glk.pl.hostingasp.pl/Services/Messenger.svc/GetFriendArchive/",query);
                Document doc = ConvertStringToXML(result);
                
                //wszystkie wiadomosci
                if(doc == null)
                    return messages;
                
                NodeList received_messages = doc.getElementsByTagName("Message");
                
                
                //iterujemy po kazdej wiadomosci
                IMessage msg;
                int sender, receiver;
                String date, content;
                for (int i = 0; i < received_messages.getLength(); i++) {
                    
                    Element e = (Element)received_messages.item(i);
                    sender = Integer.parseInt(e.getElementsByTagName("SenderGLKNumber").item(0).getTextContent());
                    receiver = Integer.parseInt(e.getElementsByTagName("ReceiverGLKNumber").item(0).getTextContent());
                    date = e.getElementsByTagName("SendDate").item(0).getTextContent();
                    content = e.getElementsByTagName("MessageCnt").item(0).getTextContent();
                    msg = new Message(sender, content, receiver, date);
                    
                    
                    messages.add(msg);             
                }
                
               
                return messages;
    }

}
