package pk.glk.configprovider.internal;

import org.osgi.framework.BundleContext;
import org.apache.felix.scr.annotations.*;

import java.lang.String;

import pk.glk.contracts.IConfig;
import pk.glk.contracts.IConfigInfo;

@Service
@Component
public final class Config
    implements IConfigInfo, IConfig
{
        private int GLKNumber;
        private String password;
        private boolean rememberLogin;
        
        BundleContext bc;
    
	@Override
	public void SetConfig(int GLKNumber, String password, boolean rememberLogin)
	{
		this.GLKNumber = GLKNumber;
                this.password = password;
                this.rememberLogin = rememberLogin;
	}
	
	@Override
	public IConfigInfo GetConfig()
	{
		return this;
	}

	@Override
	public void LogOut()
	{
		
	}

    
    @Activate
    protected void activate(BundleContext bc)
    {
        this.bc = bc;
    }

    @Override
    public int GetNumber() {
        return this.GLKNumber;
    }

    @Override
    public String GetPassword() {
        return this.password;
    }

    @Override
    public boolean GetRememberMe() {
        return this.rememberLogin;
    }
	
}

