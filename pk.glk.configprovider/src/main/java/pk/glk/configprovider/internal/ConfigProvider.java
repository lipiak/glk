package pk.glk.configprovider.internal;

import org.osgi.framework.BundleContext;
import org.apache.felix.scr.annotations.*;

import java.lang.String;

import pk.glk.contracts.IChangeSetting;
import pk.glk.contracts.IConfigInfo;
import pk.glk.configprovider.internal.Config;

/**
 * Internal implementation of our example OSGi service
 */
@Component
@Service
public final class ConfigProvider
    implements IChangeSetting
{
	private BundleContext context;
	private Config config;
	
	@Activate
	public void activate(BundleContext bc)
	{
		this.context = bc;
		System.out.println("ConfigProvider bundle activated");
		
		//create config class
		config = new Config();
	}
	
	@Override
	public void ChangeSettings(IConfigInfo ConfigToChange)
	{
		
	}
	
	@Override
	public IConfigInfo GetConfig()
	{
		return config;
	}

	@Override
	public void ResetConfig()
	{
		
	}
}

