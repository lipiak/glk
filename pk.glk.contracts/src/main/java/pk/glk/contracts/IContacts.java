package pk.glk.contracts;

import pk.glk.contracts.IContactInfo;
import java.util.List;

public interface IContacts
{  
    List<IContactInfo> GetContactList();
    List<IContactInfo> AddNewContact(int number);
    List<IContactInfo> DeleteContact(IContactInfo contact);
}




