package pk.glk.contracts;

import pk.glk.contracts.IContactInfo;
import java.util.List;

public interface IContactsController
{  
    List<IContactInfo> GetContactList();
    void AddNewContact(String number);
    Boolean DeleteContact(String number);
    
    //List<IMessage> RecieveFriendRegistry(int FriendGLKNumber);
}




