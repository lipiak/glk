package pk.glk.contracts;

import java.util.List;
import pk.glk.contracts.IContactInfo;
import pk.glk.contracts.IMessage;

public interface IContactManager
{
	Boolean DeleteContact(String number);
	void AddContact(String number);
	List<IContactInfo> ReturnContactList();
	String ReturnUserIP(IContactInfo contact);
	List<IMessage> ReturnUserArchive(IContactInfo contact);
}



