package pk.glk.contracts;

import pk.glk.contracts.IConfigInfo;

public interface IChangeSetting
{
	void ChangeSettings(IConfigInfo ConfigToChange);
	IConfigInfo GetConfig();

	void ResetConfig();
}




