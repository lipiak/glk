package pk.glk.contracts;



public interface IMessage
{  
    int GetSenderNumber();
    String GetMessage();
    int GetReceiverNumber();
    String GetSendDate();
}




