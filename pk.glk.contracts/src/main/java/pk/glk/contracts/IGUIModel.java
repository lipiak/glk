/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.glk.contracts;

import java.beans.PropertyChangeListener;
import java.util.List;

/**
 *
 * @author Patryk
 */
public interface IGUIModel {
    void RegisterPropertyChangeListener(PropertyChangeListener pcl);
    void SetLoggedIn(boolean value);
    void NewMessageReceived(IMessage msg);
    void NewConversation(int number);
    void FriendArchive(List<IMessage> msg);
}
