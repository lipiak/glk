package pk.glk.contracts;


public interface IGLKProvider
{
	int RegisterNewUser(IUser newUser);
	boolean CheckAuthorization();
	Boolean ChangeStatus(Boolean isOnline);
        String LogIn(String number, String password);
        boolean LogOut(String number, String password);
}



