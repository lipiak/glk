package pk.glk.contactscontroller.internal;

import java.util.ArrayList;
import org.osgi.framework.BundleContext;

import java.util.concurrent.atomic.AtomicReference;

import org.apache.felix.scr.annotations.*;
import java.util.List;

//provided
import pk.glk.contracts.IContactsController;

//required
import pk.glk.contracts.IContactManager;
import pk.glk.contracts.IContactInfo;
import pk.glk.contracts.IMessage;
import pk.glk.glkprovider.internal.GLK;

@Service
@Component
public final class ContactsController
    implements IContactsController
{
	private BundleContext context;
        private List<IContactInfo> contacts;
        
        public ContactsController()
        {
            contacts = new ArrayList<>();
        }
	
	@Reference
	private IContactManager ContactManager;
	
	@Activate
	public void activate(BundleContext bc)
	{
		this.context = bc;
		System.out.println("ContactsController bundle activated 22");
	}
	
	//connect ContactManager component
	protected void bindContactManager(IContactManager cm)
	{
		this.ContactManager = cm;
	}
	
	@Override
	public List<IContactInfo> GetContactList()
	{
                contacts = ContactManager.ReturnContactList();
		return contacts;
	}

    @Override
    public void AddNewContact(String number) {
       //add new contact on server
        ContactManager.AddContact(number);
    }

    @Override
    public Boolean DeleteContact(String number) {
        boolean b = ContactManager.DeleteContact(number);
        return b;
    }

}

