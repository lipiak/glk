package pk.glk.conversationcontroller.internal;

import org.osgi.framework.BundleContext;
import org.apache.felix.scr.annotations.*;
import java.lang.String;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;


//provided
import pk.glk.contracts.IConversationManager;

//required
import pk.glk.soundplayer.ISound;
import pk.glk.contracts.IEncrypte;
import pk.glk.contracts.IConfig;
import pk.glk.contracts.IConversationProvider;
import pk.glk.contracts.IBBCode;
import pk.glk.contracts.ICheckSpelling;
import pk.glk.contracts.IConfigInfo;
import pk.glk.contracts.IContactInfo;
import pk.glk.contracts.IGUIModel;
import pk.glk.contracts.IMessage;

@Service
@Component
public final class ConversationControl
    implements IConversationManager
{
	private BundleContext context;
        private IContactInfo conversationContact;
        private List<String> messages;
        private boolean bReceiving;
        private Thread receivingThread;
        
        public ConversationControl()
        {
            this.messages = new ArrayList<>();
            bReceiving = false;
        }
	
	@Reference
	private ISound Sound;	
	
	@Reference
	private IEncrypte Encryptor;	
	
	@Reference
	private IConfig Config;	
	
	@Reference
	private IConversationProvider ConversationProvider;
	
	@Reference
	private IBBCode BBCode;	
	
	//@Reference
	//private ICheckSpelling Spelling;
        
        @Reference 
        private IGUIModel GUIModel;
    
	
	//connect Sound component
	protected void bindSound(ISound sound)
	{
		this.Sound = sound;
	}
	
	//connect Encryptor component
	protected void bindEncryptor(IEncrypte e)
	{
		this.Encryptor = e;
	}
	
	//connect Config component
	protected void bindConfig(IConfig config)
	{
		this.Config = config;
	}
	
	//connect Conversation provider component
	protected void bindConversationProvider(IConversationProvider cp)
	{
		this.ConversationProvider = cp;
	}
	
	//connect BBCode component
	protected void bindBBCode(IBBCode code)
	{
		this.BBCode = code;
	}
	
	//connect Spelling component
	protected void bindSpelling(ICheckSpelling cs)
	{
		//this.Spelling = cs;
	}
        
        protected void bindGUIModel(IGUIModel model)
        {
            this.GUIModel = model;
        }
	
	@Activate
        public void activate(BundleContext bc)
        {
            System.out.print("Conversation Controller started");
        }

        @Override
        public void GetAllMessages() {
                
            System.out.println("-------- Receiving new messages -------------");
            List<IMessage> msgs = ConversationProvider.ReciveAllMessages();
            if(!msgs.isEmpty())
            {
                for(IMessage m : msgs)
                {
                    GUIModel.NewMessageReceived(m);
                }
            }
        }

//
    public void StopReceiving() {
        this.receivingThread.interrupt();
    }

    @Override
    public void SendMessage(int number, String msg) {
        IConfigInfo c = Config.GetConfig();
        IMessage m = ConversationProvider.CreateNewMessage(c.GetNumber(), msg, number, (new Date()).toString());
        boolean b = ConversationProvider.SendMessage(m);
    }
        
        

}

